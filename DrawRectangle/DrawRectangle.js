function main(){
	// 找到canvas元素，很像cc.find啊
	var canvas = document.getElementById('example')
	if(!canvas) {
		return;
	}
	// 获取上下文 绘制2D图形
	var ctx = canvas.getContext('2d');
	// 这里给canvas加入颜色，canvas本身是透明的，跟CCC的canvas还是有点不一样
	ctx.fillStyle = 'rgba(0, 0, 255, 0.5)';
	// 填充区域
	ctx.fillRect(120, 10, 150, 150);
}