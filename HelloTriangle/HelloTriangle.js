// 顶点着色器
var VSHADER_SOURCE = 
	'attribute vec4 a_Position;\n' + // 获取位置参数
	'void main() {\n' +
	'	gl_Position = a_Position;\n' +
	'}\n';
	
// 片元着色器
var FSHADER_SOURCE = 
	'void main () {\n' +
	' gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);\n' +
	'}\n';
	
function main(){
	var canvas = document.getElementById('webgl');
	var gl = getWebGLContext(canvas);
	if (!gl) {
		console.log('Failed to get the rendering context for WebGL');
		return;
	}
	// 初始化着色器
	if (!initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE)) {
		console.log('Failed to intialize shaders.');
		return;
	}
	// 使用缓冲区对象 设置顶点位置
	var n = initVertexBuffers(gl);
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	gl.clear(gl.COLOR_BUFFER_BIT);
	// 传入的绘制个数是n，所以一次性可以绘制完成
	gl.drawArrays(gl.TRIANGLES, 0, n);
}

function initVertexBuffers(gl){
	// 一个特殊的js数组 类型化数组
	var vertices= new Float32Array([
		0.0,0.5,-0.5,-0.5,0.5,-0.5
	]);
	var n = 3; // 点的个数
	// 创建缓冲区对象
	var vertexBuffer = gl.createBuffer();
	if(!vertexBuffer) {
		console.log('Failed to create the buffer object');
		return -1;
	}
	// 将缓冲区对象绑定到目标
	// gl.ARRAY_BUFFER 表示缓冲区包含了顶点的数据
	gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
	// 向缓冲区对象中写入数据 
	// gl.STATIC_DRAW 只向缓冲区写入一次数据，但需要绘制多次
	// gl.STREAM_DRAW 只写入一次，然后绘制若干次
	 // gl.DYNAMIC_DRAW 写入多次并绘制很多次
	gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);
	var a_Position = gl.getAttribLocation(gl.program, 'a_Position');
	// 将缓冲区对象分配给a_Position变量
	// gl.vertexAttrib[1234]f 只能传输一个值
	gl.vertexAttribPointer(a_Position, 2, gl.FLOAT, false, 0, 0);
	// 开启attribute变量
	// 连接a_Position变量与分配给它的缓冲区对象
	gl.enableVertexAttribArray(a_Position);
	return n;
}