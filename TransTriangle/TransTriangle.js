// 顶点着色器
var VSHADER_SOURCE = 
	'attribute vec4 a_Position;\n' + // 获取位置参数
	//'uniform vec4 u_Translation;\n' + // 获取移动位置参数
	'uniform mat4 u_xformMatrix;\n' + // 获取旋转参数
	'void main() {\n' +
	// '	gl_Position = a_Position + u_Translation;\n' +
	// 矢量与矩阵的运算是内置的
	'	gl_Position = u_xformMatrix * a_Position;\n' +
	'}\n';
	
// 片元着色器
var FSHADER_SOURCE = 
	'void main () {\n' +
	' gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);\n' +
	'}\n';
// 定义移动的距离
// var moveX = 0.5, moveY = 0.5, moveZ = 0;
// 定义旋转角度
var ANGLE = 90;
function main(){
	var canvas = document.getElementById('webgl');
	var gl = getWebGLContext(canvas);
	if (!gl) {
		console.log('Failed to get the rendering context for WebGL');
		return;
	}
	// 初始化着色器
	if (!initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE)) {
		console.log('Failed to intialize shaders.');
		return;
	}
	// 使用缓冲区对象 设置顶点位置
	var n = initVertexBuffers(gl);
	
	// // 将平移距离传输给顶点着色器
	// var u_Translation = gl.getUniformLocation(gl.program, 'u_Translation');
	// // 齐次坐标的w为0的原因是原坐标+移动坐标的w需为1，而原坐标1，所以该坐标为0.0
	// // w需要为1的原因：为1则表示该坐标为三维坐标
	// gl.uniform4f(u_Translation, moveX, moveY, moveZ, 0.0);
	
	// 转为弧度制
	var radian = Math.PI * ANGLE / 180.0;
	var cosB = Math.cos(radian);
	var sinB = Math.sin(radian);
	// WebGL矩阵是列主序的
	var xformMatrix = new Float32Array([
		cosB, sinB, 0.0, 0.0,
		-sinB, cosB, 0.0, 0.0,
		0.0, 0.0, 1.0, 0.0,
		0.0, 0.0, 0.0, 1.0
	]);
	var u_xformMatrix = gl.getUniformLocation(gl.program, 'u_xformMatrix');
	// 参数
	// locaion uniform变量的存储位置
	// Transpose 是否转置矩阵 WebGL没有提供该功能，所以必须为false
	// 待传输的类型化数组
	gl.uniformMatrix4fv(u_xformMatrix, false, xformMatrix);
	
	// 设置canvas背景色
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	gl.clear(gl.COLOR_BUFFER_BIT);
	// 传入的绘制个数是n，所以一次性可以绘制完成
	gl.drawArrays(gl.TRIANGLES, 0, n);
}

function initVertexBuffers(gl){
	// 一个特殊的js数组 类型化数组
	var vertices= new Float32Array([
		0.0,0.5,-0.5,-0.5,0.5,-0.5
	]);
	var n = 3; // 点的个数
	// 创建缓冲区对象
	var vertexBuffer = gl.createBuffer();
	if(!vertexBuffer) {
		console.log('Failed to create the buffer object');
		return -1;
	}
	// 将缓冲区对象绑定到目标
	// gl.ARRAY_BUFFER 表示缓冲区包含了顶点的数据
	gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
	// 向缓冲区对象中写入数据 
	// gl.STATIC_DRAW 只向缓冲区写入一次数据，但需要绘制多次
	// gl.STREAM_DRAW 只写入一次，然后绘制若干次
	 // gl.DYNAMIC_DRAW 写入多次并绘制很多次
	gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);
	var a_Position = gl.getAttribLocation(gl.program, 'a_Position');
	// 将缓冲区对象分配给a_Position变量
	// gl.vertexAttrib[1234]f 只能传输一个值
	gl.vertexAttribPointer(a_Position, 2, gl.FLOAT, false, 0, 0);
	// 开启attribute变量
	// 连接a_Position变量与分配给它的缓冲区对象
	gl.enableVertexAttribArray(a_Position);
	return n;
}