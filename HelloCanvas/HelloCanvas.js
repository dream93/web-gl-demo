 function main(){
	 var canvas = document.getElementById('webgl');
	 var gl = getWebGLContext(canvas);
	 if(!gl) {
		 console.log('Failed to get the rendering context for WebGL');
		 return;
	 }
	 // 设置背景色
	 // 除非下一次调用clearColor，否则背景色会一直常驻WebGL系统
	 gl.clearColor(0.0, 0.0, 0.0, 1.0);
	 // 清空绘图区【实际是清空颜色缓冲区】 
	 // 清除方式就是用上面的背景色擦除已经绘制的内容
	 // gl.COLOR_BUFFER_BIT 指定颜色缓存
	 // gl.DEPTH_BUFFER_BIT 指定深度缓冲区
	 // gl.STENCIL_BUFFER_BIT 指定模板缓冲区
	 gl.clear(gl.COLOR_BUFFER_BIT);
 }